#![allow(dead_code)]
#![allow(unused_macros)]

/// Version number of the current version of the pcap file format.
///
/// NOTE: this is *NOT* the version number of the libpcap library.
/// To fetch the version information for the version of libpcap
/// you're using, use `pcap_lib_version()`.
///
/// Source: `pcap/pcap.h`
pub const PCAP_VERSION_MAJOR : usize	= 2;
pub const PCAP_VERSION_MINOR : usize	= 4;

pub const PCAP_ERRBUF_SIZE : usize		= 256;

/// Interface is loopback
pub const PCAP_IF_LOOPBACK: usize		= 0x00000001;

/// Interface is up
pub const PCAP_IF_UP: usize				= 0x00000002;

/// Interface is running
pub const PCAP_IF_RUNNING: usize		= 0x00000004;

/// # Macro for the value returned by `pcap_datalink_ext()`.
///
/// If `lt_fcs_length_present!(x)` is true, the `lt_fcs_length!(x)` macro
/// gives the FCS length of packets in the capture.
///
/// Source: `pcap/pcap.h`
#[macro_export]
macro_rules! lt_fcs_length
{
	($x:expr) => ((($x as usize) & 0xf0000000usize) >> 28)
}

/// # Macro for the value returned by `pcap_datalink_ext()`.
///
/// If `lt_fcs_length_present!(x)` is true, the `lt_fcs_length!(x)` macro
/// gives the FCS length of packets in the capture.
///
/// Source: `pcap/pcap.h`
#[macro_export]
macro_rules! lt_fcs_length_present
{
	($x:expr) => (($x as usize) & 0x04000000usize)
}

#[macro_export]
macro_rules! lt_fcs_datalink_ext
{
	($x:expr) => (((($x as usize) & 0xf) << 28) | 0x04000000usize)
}

/// Value to pass to pcap_compile() as the netmask if you don't know what
/// the netmask is.
///
/// Source: `pcap/pcap.h`
pub const PCAP_NETMASK_UNKNOWN: usize	= 0xffffffff;

#[cfg(test)]
mod test
{
	#[test]
	fn lt_fcs_length_present_test()
	{
		assert_eq!(lt_fcs_length_present!(0x0), 0x0);
		assert_eq!(lt_fcs_length_present!(0x04000000), 0x4000000);
		assert_eq!(lt_fcs_length_present!(0xffffffff), 0x4000000);
	}

	#[test]
	fn lt_fcs_length_test()
	{
		assert_eq!(lt_fcs_length!(0x0), 0x0);
		assert_eq!(lt_fcs_length!(0x40000000), 0x4);
		assert_eq!(lt_fcs_length!(0xffffffff), 0xf);
	}

	#[test]
	fn lt_fcs_datalink_ext()
	{
		assert_eq!(lt_fcs_datalink_ext!(0x0), 0x04000000);
		assert_eq!(lt_fcs_datalink_ext!(0xf), 0xf4000000);
		assert_eq!(lt_fcs_datalink_ext!(0xffffffff), 0xf4000000);
		assert_eq!(lt_fcs_datalink_ext!(0x11111111), 0x14000000);
	}
}