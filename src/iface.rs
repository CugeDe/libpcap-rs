#![allow(dead_code)]

use addr::PcapAddr;
use libc;
use raw;
use std::ffi::CStr;
use std::error::Error;
use utils::FromRaw;

/// # Struct PcapIf
///
/// This structure is the Rust implementation of
/// the C struct pcap_if.
///
/// ## Building instance
///
/// To build a new instance, it is possible to to it with:
/// - a clean `PcapIf`
/// - a C `struct pcap_if`
///
/// ### Building clean instance
///
/// To build a new clean instance just use its constructor:
/// ```rust
/// let addr = PcapIf::new();
/// ```
///
/// ### Building instance from a C `struct pcap_if`
///
/// To build a new instance from a C `struct pcap_if`,
/// there is the method `from_raw`:
/// ```rust
/// let c_addr = unsafe { ::libc::malloc(::std::mem::size_of::<::libc::pcap_if>()) as *const ::libc::pcap_if };
///
/// ...
///
/// let addr = PcapIf::from_raw(c_addr);
/// unsafe { ::libc::free(c_addr as *mut ::libc::c_void) };
/// ```
///
/// ## Data management
///
/// Once the instance is created, there are getters / setters to
/// either get or set values.
///
#[derive(Eq, PartialEq, Debug)]
pub struct PcapIf
{
	next: Option<Box<PcapIf>>,
	name: Option<String>,
	description: Option<String>,
	addresses: Option<Box<PcapAddr>>,
	flags: u32
}

impl PcapIf
{
	pub fn new()
		-> Self
	{
		Self
		{
			next: None,
			name: None,
			description: None,
			addresses: None,
			flags: 0u32
		}
	}

	pub fn next(&self)
		-> &Option<Box<PcapIf>>
	{
		&self.next
	}

	pub fn next_mut(&mut self)
		-> &mut Option<Box<PcapIf>>
	{
		&mut self.next
	}


	pub fn name(&self)
		-> &Option<String>
	{
		&self.name
	}

	pub fn name_mut(&mut self)
		-> &mut Option<String>
	{
		&mut self.name
	}


	pub fn description(&self)
		-> &Option<String>
	{
		&self.description
	}

	pub fn description_mut(&mut self)
		-> &mut Option<String>
	{
		&mut self.description
	}


	pub fn addresses(&self)
		-> &Option<Box<PcapAddr>>
	{
		&self.addresses
	}

	pub fn addresses_mut(&mut self)
		-> &mut Option<Box<PcapAddr>>
	{
		&mut self.addresses
	}

	pub fn flags(&self)
		-> u32
	{
		self.flags
	}

	pub fn flags_mut(&mut self)
		-> &mut u32
	{
		&mut self.flags
	}
}

impl FromRaw for PcapIf {
	type RawType = raw::pcap_if;

	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
	{
		if !raw.is_null()
		{
			let mut next: Option<Box<PcapIf>> = None;
			let mut name: Option<String> = None;
			let mut description: Option<String> = None;
			let mut addresses: Option<Box<PcapAddr>> = None;

			// Lazy loading of `next`
			if !(unsafe { (*raw).next as *const Self::RawType }).is_null()
			{
				// /!\ Recusion /!\
				next = match PcapIf::from_raw( unsafe { (*raw).next as *const Self::RawType } ) {
					Ok(value) => Some(Box::new(value)),
					Err(error) => return Err(error)
				};
			}

			// Lazy loading of `name`
			if !(unsafe { (*raw).name }).is_null()
			{
				name = match unsafe { CStr::from_ptr((*raw).name as *const libc::c_char) }.to_str() {
					Ok(value) => {
						Some(value.to_string())
					},
					Err(error) => return Err(error.description().to_string())
				};
			}

			// Lazy loading of `description`
			if !(unsafe { (*raw).description }).is_null()
			{
				description = match unsafe { CStr::from_ptr((*raw).description as *const libc::c_char ) }.to_str() {
					Ok(value) => {
						Some(value.to_string())
					},
					Err(error) => return Err(error.description().to_string())
				};
			}

			// Lazy loading of `addresses`
			if !(unsafe { (*raw).addresses }).is_null()
			{
				// /!\ Recusion /!\
				addresses = match PcapAddr::from_raw( unsafe { (*raw).addresses as *const raw::pcap_addr } ) {
					Ok(value) => Some(Box::new(value)),
					Err(error) => return Err(error)
				};
			}

			let flags = unsafe { (*raw).flags } as u32;

			Ok(Self {
				next: next,
				name: name,
				description: description,
				addresses: addresses,
				flags: flags
			})
		}
		else
		{
			Err("Invalid null value".to_string())
		}
	}
}

#[cfg(test)]
mod test
{
	use addr::PcapAddr;
	use libc;
	use raw;
	use std::ffi::CString;
	use std::mem;
	use std::net::Ipv4Addr;
	use std::ptr;
	use super::PcapIf;
	use utils::FromRaw;

	#[test]
	pub fn null_pcap_if_from_raw()
	{
		let raw_pcap_if: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let next: *mut raw::pcap_if = ptr::null_mut() as *mut raw::pcap_if;
		let name: *mut libc::c_char = ptr::null_mut() as *mut libc::c_char;
		let description: *mut libc::c_char = ptr::null_mut() as *mut libc::c_char;
		let addresses: *mut raw::pcap_addr = ptr::null_mut() as *mut raw::pcap_addr;
		let flags: u32 = 0u32;

		unsafe
		{
			(*raw_pcap_if).next = next;
			(*raw_pcap_if).name = name as *mut libc::c_char;
			(*raw_pcap_if).description = description as *mut libc::c_char;
			(*raw_pcap_if).addresses = addresses as *mut raw::pcap_addr;
			(*raw_pcap_if).flags = flags as u32;
		}

		let pcap_if = PcapIf::from_raw(raw_pcap_if)
			.expect("Failed to build PcapIf from valid raw raw::pcap_if");

		assert_eq!(*pcap_if.next(), None);
		assert_eq!(*pcap_if.name(), None);
		assert_eq!(*pcap_if.description(), None);
		assert_eq!(*pcap_if.addresses(), None);
		assert_eq!(pcap_if.flags(), 0);

		let _ = unsafe { libc::free(raw_pcap_if as *mut libc::c_void) };
	}

	#[test]
	pub fn pcap_if_from_raw()
	{
		let raw_pcap_if: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let next: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let name: *mut libc::c_char = unsafe {
			libc::malloc(5 * mem::size_of::<libc::c_char>())
		} as *mut libc::c_char;

		let description: *mut libc::c_char = unsafe {
			libc::malloc(5 * mem::size_of::<libc::c_char>())
		} as *mut libc::c_char;

		let addresses: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let flags: u32 = 0u32;

		unsafe
		{
			(*next).next = ptr::null_mut() as *mut raw::pcap_if;
			(*next).name = ptr::null_mut() as *mut libc::c_char;
			(*next).description = ptr::null_mut() as *mut libc::c_char;
			(*next).addresses = ptr::null_mut() as *mut raw::pcap_addr;
			(*next).flags = 0u32;

			libc::strcpy(name, CString::new("test").expect("Failed to create CString").into_raw());
			libc::strcpy(description, CString::new("test").expect("Failed to create CString").into_raw());

			(*addresses).next = ptr::null_mut() as *mut raw::pcap_addr;
			(*addresses).addr = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).netmask = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).broadaddr = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).dstaddr = ptr::null_mut() as *mut libc::sockaddr;

			(*raw_pcap_if).next = next;
			(*raw_pcap_if).name = name as *mut libc::c_char;
			(*raw_pcap_if).description = description as *mut libc::c_char;
			(*raw_pcap_if).addresses = addresses as *mut raw::pcap_addr;
			(*raw_pcap_if).flags = flags as u32;
		}

		let pcap_if = PcapIf::from_raw(raw_pcap_if)
			.expect("Failed to build PcapIf from valid raw raw::pcap_if");

		assert_eq!(*pcap_if.next(), Some(Box::new(PcapIf::new())));
		assert_eq!(*pcap_if.name(), Some("test".to_string()));
		assert_eq!(*pcap_if.description(), Some("test".to_string()));
		assert_eq!(*pcap_if.addresses(), Some(Box::new(PcapAddr::new())));
		assert_eq!(pcap_if.flags(), 0u32);

		let _ = unsafe { libc::free(next as *mut libc::c_void) };
		let _ = unsafe { libc::free(name as *mut libc::c_void) };
		let _ = unsafe { libc::free(description as *mut libc::c_void) };
		let _ = unsafe { libc::free(addresses as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_if as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_next_pcap_if_from_raw()
	{
		let raw_pcap_if: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let next: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let name: *mut libc::c_char = unsafe {
			libc::malloc(5 * mem::size_of::<libc::c_char>())
		} as *mut libc::c_char;

		let description: *mut libc::c_char = unsafe {
			libc::malloc(5 * mem::size_of::<libc::c_char>())
		} as *mut libc::c_char;

		let addresses: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let flags: u32 = 0u32;

		unsafe
		{
			libc::strcpy(name, CString::new(vec![1, 159, 146, 150]).expect("Failed to create CString").into_raw());
			libc::strcpy(description, CString::new(vec![1, 159, 146, 150]).expect("Failed to create CString").into_raw());

			(*addresses).next = ptr::null_mut() as *mut raw::pcap_addr;
			(*addresses).addr = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).netmask = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).broadaddr = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).dstaddr = ptr::null_mut() as *mut libc::sockaddr;

			(*next).next = ptr::null_mut() as *mut raw::pcap_if;
			(*next).name = name as *mut libc::c_char;
			(*next).description = description as *mut libc::c_char;
			(*next).addresses = addresses as *mut raw::pcap_addr;
			(*next).flags = flags as u32;

			(*raw_pcap_if).next = next as *mut raw::pcap_if;
			(*raw_pcap_if).name = ptr::null_mut() as *mut libc::c_char;
			(*raw_pcap_if).description = ptr::null_mut() as *mut libc::c_char;
			(*raw_pcap_if).addresses = ptr::null_mut() as *mut raw::pcap_addr;
			(*raw_pcap_if).flags = flags as u32;
		}

		assert!(PcapIf::from_raw(raw_pcap_if).is_err());

		let _ = unsafe { libc::free(next as *mut libc::c_void) };
		let _ = unsafe { libc::free(name as *mut libc::c_void) };
		let _ = unsafe { libc::free(description as *mut libc::c_void) };
		let _ = unsafe { libc::free(addresses as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_if as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_name_pcap_if_from_raw()
	{
		let raw_pcap_if: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let name: *mut libc::c_char = unsafe {
			libc::malloc(5 * mem::size_of::<libc::c_char>())
		} as *mut libc::c_char;

		let next: *mut raw::pcap_if = ptr::null_mut() as *mut raw::pcap_if;
		let description: *mut libc::c_char = ptr::null_mut() as *mut libc::c_char;
		let addresses: *mut libc::sockaddr = ptr::null_mut() as *mut libc::sockaddr;
		let flags: u32 = 0u32;

		unsafe
		{
			libc::strcpy(name, CString::new(vec![1, 159, 146, 150]).expect("Failed to create CString").into_raw());

			(*raw_pcap_if).next = next as *mut raw::pcap_if;
			(*raw_pcap_if).name = name as *mut libc::c_char;
			(*raw_pcap_if).description = description as *mut libc::c_char;
			(*raw_pcap_if).addresses = addresses as *mut raw::pcap_addr;
			(*raw_pcap_if).flags = flags as u32;
		}

		assert!(PcapIf::from_raw(raw_pcap_if).is_err());

		let _ = unsafe { libc::free(name as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_if as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_description_pcap_if_from_raw()
	{
		let raw_pcap_if: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let description: *mut libc::c_char = unsafe {
			libc::malloc(5 * mem::size_of::<libc::c_char>())
		} as *mut libc::c_char;

		let next: *mut raw::pcap_if = ptr::null_mut() as *mut raw::pcap_if;
		let name: *mut libc::c_char = ptr::null_mut() as *mut libc::c_char;
		let addresses: *mut libc::sockaddr = ptr::null_mut() as *mut libc::sockaddr;
		let flags: u32 = 0u32;

		unsafe
		{
			libc::strcpy(description, CString::new(vec![1, 159, 146, 150]).expect("Failed to create CString").into_raw());

			(*raw_pcap_if).next = next as *mut raw::pcap_if;
			(*raw_pcap_if).name = name as *mut libc::c_char;
			(*raw_pcap_if).description = description as *mut libc::c_char;
			(*raw_pcap_if).addresses = addresses as *mut raw::pcap_addr;
			(*raw_pcap_if).flags = flags as u32;
		}

		assert!(PcapIf::from_raw(raw_pcap_if).is_err());

		let _ = unsafe { libc::free(description as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_if as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_addresses_pcap_if_from_raw()
	{
		let raw_pcap_if: *mut raw::pcap_if = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_if>())
		} as *mut raw::pcap_if;

		let addresses: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let addr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let next: *mut raw::pcap_if = ptr::null_mut() as *mut raw::pcap_if;
		let name: *mut libc::c_char = ptr::null_mut() as *mut libc::c_char;
		let description: *mut libc::c_char = ptr::null_mut() as *mut libc::c_char;
		let flags: u32 = 0u32;

		unsafe
		{
			(*addr).sin_family = (-libc::AF_INET) as u16;
			(*addr).sin_port = (0u16).to_be();
			(*addr).sin_addr.s_addr = u32::from(Ipv4Addr::new(192, 168, 0, 1));
			(*addr).sin_zero = [0u8; 8];

			(*addresses).next = ptr::null_mut() as *mut raw::pcap_addr;
			(*addresses).addr = addr as *mut libc::sockaddr;
			(*addresses).netmask = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).broadaddr = ptr::null_mut() as *mut libc::sockaddr;
			(*addresses).dstaddr = ptr::null_mut() as *mut libc::sockaddr;

			(*raw_pcap_if).next = next as *mut raw::pcap_if;
			(*raw_pcap_if).name = name as *mut libc::c_char;
			(*raw_pcap_if).description = description as *mut libc::c_char;
			(*raw_pcap_if).addresses = addresses as *mut raw::pcap_addr;
			(*raw_pcap_if).flags = flags as u32;
		}

		assert!(PcapIf::from_raw(raw_pcap_if).is_err());

		let _ = unsafe { libc::free(addr as *mut libc::c_void) };
		let _ = unsafe { libc::free(addresses as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_if as *mut libc::c_void) };
	}
}