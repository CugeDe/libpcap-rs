#[allow(dead_code)]

use libc;

/// # struct pcap_addr
///
/// This structure is the raw Rust binding of
/// the C struct pcap_addr.
///
#[repr(C)]
#[derive(Eq, PartialEq, Copy, Clone)]
pub struct pcap_addr {
	pub next: *mut pcap_addr,
	pub addr: *mut libc::sockaddr,
	pub netmask: *mut libc::sockaddr,
	pub broadaddr: *mut libc::sockaddr,
	pub dstaddr: *mut libc::sockaddr,
}

/// # struct pcap_if
///
/// This structure is the raw Rust binding of
/// the C struct pcap_if.
///
#[repr(C)]
#[derive(Eq, PartialEq, Copy, Clone)]
pub struct pcap_if {
	pub next: *mut pcap_if,
	pub name: *mut libc::c_char,
	pub description: *mut libc::c_char,
	pub addresses: *mut pcap_addr,
	pub flags: u32
}