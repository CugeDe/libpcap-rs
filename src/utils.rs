use libc;
use std::marker;
use std::net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6};

/// # FromRaw
///
/// Trait to implement a method to create an instance based on 
/// the C raw struct data.
///
pub trait FromRaw
{
	type RawType;

	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
		where
			Self: marker::Sized;
}

impl FromRaw for Ipv4Addr
{
	type RawType = libc::in_addr;
	
	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
	{
		if !raw.is_null()
		{
			let ip = unsafe { ((*raw).s_addr) } as u32;

			Ok(Self::from(ip))
		}
		else
		{
			Err("Invalid null value".to_string())
		}
	}
}

impl FromRaw for Ipv6Addr
{
	type RawType = libc::in6_addr;
	
	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
	{
		if !raw.is_null()
		{
			let ip = unsafe { ((*raw).s6_addr) } as [u8; 16];

			Ok(Self::from(ip))
		}
		else
		{
			Err("Invalid null value".to_string())
		}
	}
}

/// # Implement transformation of `libc::sockaddr_in` to SocketAddrV4
///
/// ## struct sockaddr_in
/// struct sockaddr_in {
///     short            sin_family;   // AF_INET
///     unsigned short   sin_port;     // port number (big endian)
///     struct in_addr   sin_addr;     // see struct in_addr above
///     char             sin_zero[8];
/// };
///
impl FromRaw for SocketAddrV4
{
	type RawType = libc::sockaddr_in;
	
	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
	{
		if !raw.is_null()
		{
			if unsafe { (*raw).sin_family } == libc::AF_INET as u16
			{
				let ip = Ipv4Addr::from_raw(&(unsafe { (*raw).sin_addr }) as *const libc::in_addr).unwrap();

				// Used to convert port (big endian) to little_endian
				let port = (unsafe { (*raw).sin_port } as u16).swap_bytes();

				Ok(Self::new(ip, port))
			}
			else
			{
				Err("Invalid libc::sockaddr_in.sin_family value".to_string())
			}
		}
		else
		{
			Err("Invalid null value".to_string())
		}
	}
}

/// # Implement transformation of `libc::sockaddr_in` to SocketAddrV4
///
/// ## struct sockaddr_in6
/// struct sockaddr_in6 {
///     u_int16_t       sin6_family;   // AF_INET6
///     u_int16_t       sin6_port;     // port number (big endian)
///     u_int32_t       sin6_flowinfo; // IPv6 flow information
///     struct in6_addr sin6_addr;     // see struct in6_addr above
///     u_int32_t       sin6_scope_id; // Scope ID
/// };
///
impl FromRaw for SocketAddrV6
{
	type RawType = libc::sockaddr_in6;
	
	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
	{
		if !raw.is_null()
		{
			if unsafe { (*raw).sin6_family } == libc::AF_INET6 as u16
			{
				let ip = Ipv6Addr::from_raw(&(unsafe { (*raw).sin6_addr }) as *const libc::in6_addr).unwrap();
				let flow_info = unsafe { (*raw).sin6_flowinfo } as u32;
				let scope_id = unsafe { (*raw).sin6_scope_id } as u32;

				// Used to convert port (big endian) to little_endian
				let port = (unsafe { (*raw).sin6_port } as u16).swap_bytes();

				Ok(Self::new(ip, port, flow_info, scope_id))
			}
			else
			{
				Err("Invalid libc::sockaddr_in6.sin6_family value".to_string())
			}
		}
		else
		{
			Err("Invalid null value".to_string())
		}
	}
}

impl FromRaw for SocketAddr
{
	type RawType = libc::sockaddr;
	
	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
	{
		if !raw.is_null()
		{
			match unsafe { (*raw).sa_family } as i32
			{
				libc::AF_INET => {
					match SocketAddrV4::from_raw(raw as *const libc::sockaddr_in)
					{
						Ok(value) => {
							Ok(SocketAddr::from(value))
						}
						Err(err) => Err(err)
					}
				},
				libc::AF_INET6 => {
					match SocketAddrV6::from_raw(raw as *const libc::sockaddr_in6)
					{
						Ok(value) => {
							Ok(SocketAddr::from(value))
						}
						Err(err) => Err(err)
					}
				},
				_ => Err("Unknown libc::sockaddr.sin_family value".to_string())
			}
		}
		else
		{
			Err("Invalid null value".to_string())
		}
	}
}

#[cfg(test)]
mod test
{
	use libc;
	use std::mem;
	use std::net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6};
	use std::ptr;
	use super::FromRaw;

	#[test]
	fn valid_ipv4addr_from_raw()
	{
		let in_addr: *mut libc::in_addr = unsafe {
			libc::malloc(mem::size_of::<libc::in_addr>())
		} as *mut libc::in_addr;

		unsafe { (*in_addr).s_addr = u32::from(Ipv4Addr::new(127, 0, 0, 1)) };
		let ipv4addr = Ipv4Addr::from_raw(in_addr as *const libc::in_addr)
			.expect("Failed to build Ipv4Addr from valid raw libc::in_addr");
		assert_eq!(ipv4addr, Ipv4Addr::new(127, 0, 0, 1));

		unsafe { (*in_addr).s_addr = u32::from(Ipv4Addr::new(255, 255, 255, 255)) };
		let ipv4addr = Ipv4Addr::from_raw(in_addr as *const libc::in_addr)
			.expect("Failed to build Ipv4Addr from valid raw libc::in_addr");
		assert_eq!(ipv4addr, Ipv4Addr::new(255, 255, 255, 255));

		unsafe { (*in_addr).s_addr = u32::from(Ipv4Addr::new(0, 0, 0, 0)) };
		let ipv4addr = Ipv4Addr::from_raw(in_addr as *const libc::in_addr)
			.expect("Failed to build Ipv4Addr from valid raw libc::in_addr");
		assert_eq!(ipv4addr, Ipv4Addr::new(0, 0, 0, 0));

		let _ = unsafe { libc::free(in_addr as *mut libc::c_void) };
	}

	#[test]
	#[should_panic]
	fn invalid_ipv4addr_from_raw()
	{
		Ipv4Addr::from_raw(ptr::null()).unwrap();
	}

	#[test]
	fn valid_ipv6addr_from_raw()
	{
		let in6_addr: *mut libc::in6_addr = unsafe {
			libc::malloc(mem::size_of::<libc::in6_addr>())
		} as *mut libc::in6_addr;

		unsafe { (*in6_addr).s6_addr = Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1).octets() };
		let ipv6addr = Ipv6Addr::from_raw(in6_addr as *const libc::in6_addr)
			.expect("Failed to build Ipv6Addr from valid raw libc::in6_addr");
		assert_eq!(ipv6addr, Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1));

		unsafe { (*in6_addr).s6_addr = Ipv6Addr::new(0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff).octets() };
		let ipv6addr = Ipv6Addr::from_raw(in6_addr as *const libc::in6_addr)
			.expect("Failed to build Ipv6Addr from valid raw libc::in6_addr");
		assert_eq!(ipv6addr, Ipv6Addr::new(0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff));

		unsafe { (*in6_addr).s6_addr = Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0).octets() };
		let ipv6addr = Ipv6Addr::from_raw(in6_addr as *const libc::in6_addr)
			.expect("Failed to build Ipv6Addr from valid raw libc::in6_addr");
		assert_eq!(ipv6addr, Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 0));

		let _ = unsafe { libc::free(in6_addr as *mut libc::c_void) };
	}

	#[test]
	#[should_panic]
	fn invalid_ipv6addr_from_raw()
	{
		Ipv6Addr::from_raw(ptr::null()).unwrap();
	}

	#[test]
	fn socketaddrv4_from_raw()
	{
		let sockaddrv4: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		unsafe
		{
			(*sockaddrv4).sin_family = libc::AF_INET as u16;
			(*sockaddrv4).sin_port = (8080u16).to_be();
			(*sockaddrv4).sin_addr.s_addr = u32::from(Ipv4Addr::new(127, 0, 0, 1));
			(*sockaddrv4).sin_zero = [0u8; 8];
		}

		let socketaddrv4 = SocketAddrV4::from_raw(sockaddrv4 as *const libc::sockaddr_in)
			.expect("Failed to build SocketAddrV4 from valid raw libc::sockaddr_in");

		assert_eq!(socketaddrv4.port(), 8080u16);
		assert_eq!(*socketaddrv4.ip(), Ipv4Addr::new(127, 0, 0, 1));

		let _ = unsafe { libc::free(sockaddrv4 as *mut libc::c_void) };
	}

	#[test]
	fn invalid_socketaddrv4_from_raw()
	{
		let sockaddrv4: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		unsafe
		{
			(*sockaddrv4).sin_family = libc::AF_INET6 as u16;
			(*sockaddrv4).sin_port = (8080u16).to_be();
			(*sockaddrv4).sin_addr.s_addr = u32::from(Ipv4Addr::new(127, 0, 0, 1));
			(*sockaddrv4).sin_zero = [0u8; 8];	
		}

		let socketaddrv = SocketAddrV4::from_raw(sockaddrv4 as *const libc::sockaddr_in);
		assert!(socketaddrv.is_err());

		let socketaddrv = SocketAddrV4::from_raw(ptr::null() as *const libc::sockaddr_in);
		assert!(socketaddrv.is_err());

		let _ = unsafe { libc::free(sockaddrv4 as *mut libc::c_void) };
	}

	#[test]
	fn socketaddrv6_from_raw()
	{
		let sockaddrv6: *mut libc::sockaddr_in6 = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in6>())
		} as *mut libc::sockaddr_in6;

		unsafe
		{
			(*sockaddrv6).sin6_family = libc::AF_INET6 as u16;
			(*sockaddrv6).sin6_port = (8080u16).to_be();
			(*sockaddrv6).sin6_flowinfo = 0u32;
			(*sockaddrv6).sin6_addr.s6_addr = Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1).octets();
			(*sockaddrv6).sin6_scope_id = 0u32;
		}

		let socketaddrv6 = SocketAddrV6::from_raw(sockaddrv6 as *const libc::sockaddr_in6)
			.expect("Failed to build SocketAddrV6 from valid raw libc::sockaddr_in6");

		assert_eq!(socketaddrv6.port(), 8080u16);
		assert_eq!(socketaddrv6.flowinfo(), 0u32);
		assert_eq!(socketaddrv6.scope_id(), 0u32);
		assert_eq!(*socketaddrv6.ip(), Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1));

		let _ = unsafe { libc::free(sockaddrv6 as *mut libc::c_void) };
	}

	#[test]
	fn invalid_socketaddrv6_from_raw()
	{
		let sockaddrv6: *mut libc::sockaddr_in6 = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in6>())
		} as *mut libc::sockaddr_in6;

		unsafe
		{
			(*sockaddrv6).sin6_family = libc::AF_INET as u16;
			(*sockaddrv6).sin6_port = (8080u16).to_be();
			(*sockaddrv6).sin6_flowinfo = 0u32;
			(*sockaddrv6).sin6_addr.s6_addr = Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1).octets();
			(*sockaddrv6).sin6_scope_id = 0u32;
		}

		let socketaddrv6 = SocketAddrV6::from_raw(sockaddrv6 as *const libc::sockaddr_in6);
		assert!(socketaddrv6.is_err());

		let socketaddrv6 = SocketAddrV6::from_raw(ptr::null() as *const libc::sockaddr_in6);
		assert!(socketaddrv6.is_err());

		let _ = unsafe { libc::free(sockaddrv6 as *mut libc::c_void) };
	}

	#[test]
	fn socketaddr_from_raw()
	{
		let sockaddrv4: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let sockaddrv6: *mut libc::sockaddr_in6 = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in6>())
		} as *mut libc::sockaddr_in6;

		unsafe
		{
			(*sockaddrv4).sin_family = libc::AF_INET as u16;
			(*sockaddrv4).sin_port = (8080u16).to_be();
			(*sockaddrv4).sin_addr.s_addr = u32::from(Ipv4Addr::new(127, 0, 0, 1));
			(*sockaddrv4).sin_zero = [0u8; 8];

			(*sockaddrv6).sin6_family = libc::AF_INET6 as u16;
			(*sockaddrv6).sin6_port = (8080u16).to_be();
			(*sockaddrv6).sin6_flowinfo = 0u32;
			(*sockaddrv6).sin6_addr.s6_addr = Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1).octets();
			(*sockaddrv6).sin6_scope_id = 0u32;
		}

		let sockaddr: *mut libc::sockaddr = sockaddrv4 as *mut libc::sockaddr;
		let socketaddr = SocketAddr::from_raw(sockaddr as *const libc::sockaddr)
			.expect("Failed to build SocketAddr from valid raw libc::sockaddr representing libc::sockaddr_in");

		let tester: SocketAddr = SocketAddr::from(SocketAddrV4::new(Ipv4Addr::new(127, 0, 0, 1), 8080u16));
		assert_eq!(socketaddr, tester);

		let sockaddr: *mut libc::sockaddr = sockaddrv6 as *mut libc::sockaddr;
		let socketaddr = SocketAddr::from_raw(sockaddr as *const libc::sockaddr)
			.expect("Failed to build SocketAddr from valid raw libc::sockaddr representing libc::sockaddr_in6");

		let tester: SocketAddr = SocketAddr::from(SocketAddrV6::new(Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1), 8080u16, 0u32, 0u32));
		assert_eq!(socketaddr, tester);

		let _ = unsafe { libc::free(sockaddrv4 as *mut libc::c_void) };
		let _ = unsafe { libc::free(sockaddrv6 as *mut libc::c_void) };
	}

	#[test]
	fn invalid_socketaddr_from_raw()
	{
		let sockaddrv4: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let sockaddrv6: *mut libc::sockaddr_in6 = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in6>())
		} as *mut libc::sockaddr_in6;

		unsafe
		{
			(*sockaddrv4).sin_family = (-libc::AF_INET) as u16;
			(*sockaddrv4).sin_port = (8080u16).to_be();
			(*sockaddrv4).sin_addr.s_addr = u32::from(Ipv4Addr::new(127, 0, 0, 1));
			(*sockaddrv4).sin_zero = [0u8; 8];

			(*sockaddrv6).sin6_family = (-libc::AF_INET6) as u16;
			(*sockaddrv6).sin6_port = (8080u16).to_be();
			(*sockaddrv6).sin6_flowinfo = 0u32;
			(*sockaddrv6).sin6_addr.s6_addr = Ipv6Addr::new(0, 0, 0, 0, 0, 0, 0, 1).octets();
			(*sockaddrv6).sin6_scope_id = 0u32;
		}

		let sockaddr: *mut libc::sockaddr = sockaddrv4 as *mut libc::sockaddr;
		assert!(SocketAddr::from_raw(sockaddr as *const libc::sockaddr).is_err());

		let sockaddr: *mut libc::sockaddr = sockaddrv6 as *mut libc::sockaddr;
		assert!(SocketAddr::from_raw(sockaddr as *const libc::sockaddr).is_err());

		assert!(SocketAddr::from_raw(ptr::null()).is_err());

		let _ = unsafe { libc::free(sockaddrv4 as *mut libc::c_void) };
		let _ = unsafe { libc::free(sockaddrv6 as *mut libc::c_void) };
	}
}