#![allow(dead_code)]

/// # Time stamp types.
/// Not all systems and interfaces will necessarily support all of these.
///
/// A system that supports `PcapTstamp::Host` is offering time stamps
/// provided by the host machine, rather than by the capture device,
/// but not committing to any characteristics of the time stamp;
/// it will not offer any of the `PcapTstamp::HostLowprec` and `PcapTstamp::HostHiprec` subtypes.
///
/// `PcapTstamp::HostLowprec` is a time stamp, provided by the host machine,
/// that's low-precision but relatively cheap to fetch; it's normally done
/// using the system clock, so it's normally synchronized with times you'd
/// fetch from system calls.
///
/// `PcapTstamp::HostHiprec` is a time stamp, provided by the host machine,
/// that's high-precision; it might be more expensive to fetch.  It might
/// or might not be synchronized with the system clock, and might have
/// problems with time stamps for packets received on different CPUs,
/// depending on the platform.
///
/// `PcapTstamp::Adapter` is a high-precision time stamp supplied by the
/// capture device; it's synchronized with the system clock.
///
/// `PcapTstamp::AdaoterUnsynced` is a high-precision time stamp supplied by
/// the capture device; it's not synchronized with the system clock.
///
/// _Note that time stamps synchronized with the system clock can go
/// backwards, as the system clock can go backwards.  If a clock is
/// not in sync with the system clock, that could be because the
/// system clock isn't keeping accurate time, because the other
/// clock isn't keeping accurate time, or both._
///
/// _Note that host-provided time stamps generally correspond to the
/// time when the time-stamping code sees the packet; this could
/// be some unknown amount of time after the first or last bit of
/// the packet is received by the network adapter, due to batching
/// of interrupts for packet arrival, queueing delays, etc.._
///
/// Source: `pcap/pcap.h`
#[derive(Eq, PartialEq, Debug)]
pub enum PcapTstamp {
	Host = 0,
	HostLowprec = 1,
	HostHiprec = 2,
	Adapter = 3,
	AdapterUnsynced = 4
}

/// # Time stamp resolution types.
/// Not all systems and interfaces will necessarily support all of these
/// resolutions when doing live captures; all of them can be requested
/// when reading a savefile.
///
/// Source: `pcap/pcap.h`
#[derive(Eq, PartialEq, Debug)]
pub enum PcapTstampResolution {
	PrecisionMicro = 0,
	PrecisionNano = 1
}

#[cfg(test)]
mod test
{
    use super::PcapTstamp as PTS;
    use super::PcapTstampResolution as PTSR;

    #[test]
    fn timestamp_eq()
    {
        assert_eq!(PTS::Host as i32, 0);
        assert_eq!(PTS::AdapterUnsynced as i32, 4);

        assert_eq!(PTS::Host, PTS::Host);
        assert_eq!(PTS::AdapterUnsynced, PTS::AdapterUnsynced);

        assert!(PTS::Host != PTS::AdapterUnsynced);
    }

    #[test]
    fn timestamp_resolution_eq()
    {
        assert_eq!(PTSR::PrecisionMicro as i32, 0);
        assert_eq!(PTSR::PrecisionNano as i32, 1);

        assert_eq!(PTSR::PrecisionMicro, PTSR::PrecisionMicro);
        assert_eq!(PTSR::PrecisionNano, PTSR::PrecisionNano);

        assert!(PTSR::PrecisionMicro != PTSR::PrecisionNano);
    }
}