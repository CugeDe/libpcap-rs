#![allow(dead_code)]

use libc;
use raw;
use std::net::SocketAddr;
use utils::FromRaw;

/// # Struct PcapAddr
///
/// This structure is the Rust implementation of
/// the C struct pcap_addr.
///
/// ## Building instance
///
/// To build a new instance, it is possible to to it with:
/// - a clean `PcapAddr`
/// - a C `struct pcap_addr`
///
/// ### Building clean instance
///
/// To build a new clean instance just use its constructor:
/// ```rust
/// let addr = PcapAddr::new();
/// ```
///
/// ### Building instance from a C `struct pcap_addr`
///
/// To build a new instance from a C `struct pcap_addr`,
/// there is the method `from_raw`:
/// ```rust
/// let c_addr = unsafe { ::libc::malloc(::std::mem::size_of::<::raw::pcap_addr>()) as *const ::raw::pcap_addr };
///
/// ...
///
/// let addr = PcapAddr::from_raw(c_addr);
/// unsafe { ::libc::free(c_addr as *mut ::libc::c_void) };
/// ```
///
/// ## Data management
///
/// Once the instance is created, there are getters / setters to
/// either get or set values.
///
#[derive(Eq, PartialEq, Debug)]
pub struct PcapAddr
{
	next: Option<Box<PcapAddr>>,
	addr: Option<SocketAddr>,
	netmask: Option<SocketAddr>,
	broadaddr: Option<SocketAddr>,
	dstaddr: Option<SocketAddr>
}

impl PcapAddr
{
	/// Build a new PcapAddr instance
	pub fn new()
		-> Self
	{
		Self
		{
			next: None,
			addr: None,
			netmask: None,
			broadaddr: None,
			dstaddr: None
		}
	}


	/// Returns a reference to the element `next`
	pub fn next(&self)
		-> &Option<Box<PcapAddr>>
	{
		&self.next
	}

	/// Returns a reference to the element `next`
	pub fn next_mut(&mut self)
		-> &mut Option<Box<PcapAddr>>
	{
		&mut self.next
	}


	/// Returns a reference to the element `addr`
	pub fn addr(&self)
		-> &Option<SocketAddr>
	{
		&self.addr
	}

	/// Returns a reference to the element `addr`
	pub fn addr_mut(&mut self)
		-> &mut Option<SocketAddr>
	{
		&mut self.addr
	}


	/// Returns a reference to the element `netmask`
	pub fn netmask(&self)
		-> &Option<SocketAddr>
	{
		&self.netmask
	}

	/// Returns a reference to the element `netmask`
	pub fn netmask_mut(&mut self)
		-> &mut Option<SocketAddr>
	{
		&mut self.netmask
	}


	/// Returns a reference to the element `broadaddr`
	pub fn broadaddr(&self)
		-> &Option<SocketAddr>
	{
		&self.broadaddr
	}

	/// Returns a reference to the element `broadaddr`
	pub fn broadaddr_mut(&mut self)
		-> &mut Option<SocketAddr>
	{
		&mut self.broadaddr
	}


	/// Returns a reference to the element `dstaddr`
	pub fn dstaddr(&self)
		-> &Option<SocketAddr>
	{
		&self.dstaddr
	}

	/// Returns a reference to the element `dstaddr`
	pub fn dstaddr_mut(&mut self)
		-> &mut Option<SocketAddr>
	{
		&mut self.dstaddr
	}
}

impl FromRaw for PcapAddr {
	type RawType = raw::pcap_addr;

	fn from_raw(raw: *const Self::RawType)
		-> Result<Self, String>
	{
		if !raw.is_null()
		{
			let mut next: Option<Box<PcapAddr>> = None;
			let mut addr: Option<SocketAddr> = None;
			let mut netmask: Option<SocketAddr> = None;
			let mut broadaddr: Option<SocketAddr> = None;
			let mut dstaddr: Option<SocketAddr> = None;

			// Lazy loading of `next`
			if !(unsafe { (*raw).next as *const Self::RawType }).is_null()
			{
				// /!\ Recusion /!\
				next = match PcapAddr::from_raw( unsafe { (*raw).next as *const Self::RawType } ) {
					Ok(value) => Some(Box::new(value)),
					Err(error) => return Err(error)
				};
			}

			// Lazy loading of `addr`
			if !(unsafe { (*raw).addr }).is_null()
			{
				addr = match SocketAddr::from_raw( unsafe { (*raw).addr as *const libc::sockaddr } ) {
					Ok(value) => Some(value),
					Err(error) => return Err(error)
				};
			}

			// Lazy loading of `netmask`
			if !(unsafe { (*raw).netmask }).is_null()
			{
				netmask = match SocketAddr::from_raw( unsafe { (*raw).netmask as *const libc::sockaddr } ) {
					Ok(value) => Some(value),
					Err(error) => return Err(error)
				};
			}

			// Lazy loading of `broadaddr`
			if !(unsafe { (*raw).broadaddr }).is_null()
			{
				broadaddr = match SocketAddr::from_raw( unsafe { (*raw).broadaddr as *const libc::sockaddr } ) {
					Ok(value) => Some(value),
					Err(error) => return Err(error)
				};
			}

			// Lazy loading of `dstaddr`
			if !(unsafe { (*raw).dstaddr }).is_null()
			{
				dstaddr = match SocketAddr::from_raw( unsafe { (*raw).dstaddr as *const libc::sockaddr } ) {
					Ok(value) => Some(value),
					Err(error) => return Err(error)
				};
			}

			Ok(Self {
				next: next,
				addr: addr,
				netmask: netmask,
				broadaddr: broadaddr,
				dstaddr: dstaddr
			})
		}
		else
		{
			Err("Invalid null value".to_string())
		}
	}
}

#[cfg(test)]
mod test
{
	use libc;
	use raw;
	use std::mem;
	use std::net::{Ipv4Addr, SocketAddr, SocketAddrV4};
	use std::ptr;
	use super::PcapAddr;
	use utils::FromRaw;

	#[test]
	pub fn null_pcap_addr_from_raw()
	{
		let raw_pcap_addr: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let next: *mut raw::pcap_addr = ptr::null_mut() as *mut raw::pcap_addr;
		let addr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let netmask: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let broadaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let dstaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;

		unsafe
		{
			(*raw_pcap_addr).next = next as *mut raw::pcap_addr;
			(*raw_pcap_addr).addr = addr as *mut libc::sockaddr;
			(*raw_pcap_addr).netmask = netmask as *mut libc::sockaddr;
			(*raw_pcap_addr).broadaddr = broadaddr as *mut libc::sockaddr;
			(*raw_pcap_addr).dstaddr = dstaddr as *mut libc::sockaddr;
		}

		let pcap_addr = PcapAddr::from_raw(raw_pcap_addr)
			.expect("Failed to build PcapAddr from valid raw raw::pcap_addr");

		assert_eq!(*pcap_addr.addr(), None);
		assert_eq!(*pcap_addr.netmask(), None);
		assert_eq!(*pcap_addr.broadaddr(), None);
		assert_eq!(*pcap_addr.dstaddr(), None);
		assert_eq!(*pcap_addr.next(), None);

		let _ = unsafe { libc::free(raw_pcap_addr as *mut libc::c_void) };
	}

	#[test]
	pub fn pcap_addr_from_raw()
	{
		let raw_pcap_addr: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let next: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let addr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let netmask: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let broadaddr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let dstaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;

		unsafe
		{
			(*addr).sin_family = libc::AF_INET as u16;
			(*addr).sin_port = (0u16).to_be();
			(*addr).sin_addr.s_addr = u32::from(Ipv4Addr::new(192, 168, 0, 1));
			(*addr).sin_zero = [0u8; 8];

			(*netmask).sin_family = libc::AF_INET as u16;
			(*netmask).sin_port = (0u16).to_be();
			(*netmask).sin_addr.s_addr = u32::from(Ipv4Addr::new(255, 255, 255, 0));
			(*netmask).sin_zero = [0u8; 8];

			(*broadaddr).sin_family = libc::AF_INET as u16;
			(*broadaddr).sin_port = (0u16).to_be();
			(*broadaddr).sin_addr.s_addr = u32::from(Ipv4Addr::new(255, 255, 255, 255));
			(*broadaddr).sin_zero = [0u8; 8];

			(*next).next = ptr::null_mut() as *mut raw::pcap_addr;
			(*next).addr = ptr::null_mut() as *mut libc::sockaddr;
			(*next).netmask = ptr::null_mut() as *mut libc::sockaddr;
			(*next).broadaddr = ptr::null_mut() as *mut libc::sockaddr;
			(*next).dstaddr = ptr::null_mut() as *mut libc::sockaddr;

			(*raw_pcap_addr).next = next as *mut raw::pcap_addr;
			(*raw_pcap_addr).addr = addr as *mut libc::sockaddr;
			(*raw_pcap_addr).netmask = netmask as *mut libc::sockaddr;
			(*raw_pcap_addr).broadaddr = broadaddr as *mut libc::sockaddr;
			(*raw_pcap_addr).dstaddr = dstaddr as *mut libc::sockaddr;
		}

		let pcap_addr = PcapAddr::from_raw(raw_pcap_addr)
			.expect("Failed to build PcapAddr from valid raw raw::pcap_addr");

		assert_eq!(*pcap_addr.addr(), Some(SocketAddr::from(SocketAddrV4::new(Ipv4Addr::new(192, 168, 0, 1), 0u16))));
		assert_eq!(*pcap_addr.netmask(), Some(SocketAddr::from(SocketAddrV4::new(Ipv4Addr::new(255, 255, 255, 0), 0u16))));
		assert_eq!(*pcap_addr.broadaddr(), Some(SocketAddr::from(SocketAddrV4::new(Ipv4Addr::new(255, 255, 255, 255), 0u16))));
		assert_eq!(*pcap_addr.dstaddr(), None);
		assert_eq!(*pcap_addr.next(), Some(Box::new(PcapAddr::new())));

		let _ = unsafe { libc::free(next as *mut libc::c_void) };
		let _ = unsafe { libc::free(addr as *mut libc::c_void) };
		let _ = unsafe { libc::free(netmask as *mut libc::c_void) };
		let _ = unsafe { libc::free(broadaddr as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_addr as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_addr_pcap_addr_from_raw()
	{
		let raw_pcap_addr: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let addr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let next: *mut raw::pcap_addr = ptr::null_mut() as *mut raw::pcap_addr;
		let netmask: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let broadaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let dstaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;

		unsafe
		{
			(*addr).sin_family = (-libc::AF_INET) as u16;
			(*addr).sin_port = (0u16).to_be();
			(*addr).sin_addr.s_addr = u32::from(Ipv4Addr::new(192, 168, 0, 1));
			(*addr).sin_zero = [0u8; 8];

			(*raw_pcap_addr).next = next as *mut raw::pcap_addr;
			(*raw_pcap_addr).addr = addr as *mut libc::sockaddr;
			(*raw_pcap_addr).netmask = netmask as *mut libc::sockaddr;
			(*raw_pcap_addr).broadaddr = broadaddr as *mut libc::sockaddr;
			(*raw_pcap_addr).dstaddr = dstaddr as *mut libc::sockaddr;
		}

		assert!(PcapAddr::from_raw(raw_pcap_addr).is_err());

		let _ = unsafe { libc::free(addr as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_addr as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_netmask_pcap_addr_from_raw()
	{
		let raw_pcap_addr: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let netmask: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let next: *mut raw::pcap_addr = ptr::null_mut() as *mut raw::pcap_addr;
		let addr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let broadaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let dstaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;

		unsafe
		{
			(*netmask).sin_family = (-libc::AF_INET) as u16;
			(*netmask).sin_port = (0u16).to_be();
			(*netmask).sin_addr.s_addr = u32::from(Ipv4Addr::new(255, 255, 255, 0));
			(*netmask).sin_zero = [0u8; 8];

			(*raw_pcap_addr).next = next as *mut raw::pcap_addr;
			(*raw_pcap_addr).addr = addr as *mut libc::sockaddr;
			(*raw_pcap_addr).netmask = netmask as *mut libc::sockaddr;
			(*raw_pcap_addr).broadaddr = broadaddr as *mut libc::sockaddr;
			(*raw_pcap_addr).dstaddr = dstaddr as *mut libc::sockaddr;
		}

		assert!(PcapAddr::from_raw(raw_pcap_addr).is_err());

		let _ = unsafe { libc::free(netmask as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_addr as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_broadaddr_pcap_addr_from_raw()
	{
		let raw_pcap_addr: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let broadaddr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let next: *mut raw::pcap_addr = ptr::null_mut() as *mut raw::pcap_addr;
		let addr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let netmask: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let dstaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;

		unsafe
		{
			(*broadaddr).sin_family = (-libc::AF_INET) as u16;
			(*broadaddr).sin_port = (0u16).to_be();
			(*broadaddr).sin_addr.s_addr = u32::from(Ipv4Addr::new(255, 255, 255, 255));
			(*broadaddr).sin_zero = [0u8; 8];

			(*raw_pcap_addr).next = next as *mut raw::pcap_addr;
			(*raw_pcap_addr).addr = addr as *mut libc::sockaddr;
			(*raw_pcap_addr).netmask = netmask as *mut libc::sockaddr;
			(*raw_pcap_addr).broadaddr = broadaddr as *mut libc::sockaddr;
			(*raw_pcap_addr).dstaddr = dstaddr as *mut libc::sockaddr;
		}

		assert!(PcapAddr::from_raw(raw_pcap_addr).is_err());

		let _ = unsafe { libc::free(broadaddr as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_addr as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_dstaddr_pcap_addr_from_raw()
	{
		let raw_pcap_addr: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let dstaddr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let next: *mut raw::pcap_addr = ptr::null_mut() as *mut raw::pcap_addr;
		let addr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let netmask: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;
		let broadaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;

		unsafe
		{
			(*dstaddr).sin_family = (-libc::AF_INET) as u16;
			(*dstaddr).sin_port = (0u16).to_be();
			(*dstaddr).sin_addr.s_addr = u32::from(Ipv4Addr::new(10, 0, 0, 2));
			(*dstaddr).sin_zero = [0u8; 8];

			(*raw_pcap_addr).next = next as *mut raw::pcap_addr;
			(*raw_pcap_addr).addr = addr as *mut libc::sockaddr;
			(*raw_pcap_addr).netmask = netmask as *mut libc::sockaddr;
			(*raw_pcap_addr).broadaddr = broadaddr as *mut libc::sockaddr;
			(*raw_pcap_addr).dstaddr = dstaddr as *mut libc::sockaddr;
		}

		assert!(PcapAddr::from_raw(raw_pcap_addr).is_err());

		let _ = unsafe { libc::free(dstaddr as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_addr as *mut libc::c_void) };
	}

	#[test]
	pub fn invalid_next_pcap_addr_from_raw()
	{
		let raw_pcap_addr: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let next: *mut raw::pcap_addr = unsafe {
			libc::malloc(mem::size_of::<raw::pcap_addr>())
		} as *mut raw::pcap_addr;

		let addr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let netmask: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let broadaddr: *mut libc::sockaddr_in = unsafe {
			libc::malloc(mem::size_of::<libc::sockaddr_in>())
		} as *mut libc::sockaddr_in;

		let dstaddr: *mut libc::sockaddr_in = ptr::null_mut() as *mut libc::sockaddr_in;

		unsafe
		{
			(*addr).sin_family = (-libc::AF_INET) as u16;
			(*addr).sin_port = (0u16).to_be();
			(*addr).sin_addr.s_addr = u32::from(Ipv4Addr::new(192, 168, 0, 1));
			(*addr).sin_zero = [0u8; 8];

			(*netmask).sin_family = (-libc::AF_INET) as u16;
			(*netmask).sin_port = (0u16).to_be();
			(*netmask).sin_addr.s_addr = u32::from(Ipv4Addr::new(255, 255, 255, 0));
			(*netmask).sin_zero = [0u8; 8];

			(*broadaddr).sin_family = (-libc::AF_INET) as u16;
			(*broadaddr).sin_port = (0u16).to_be();
			(*broadaddr).sin_addr.s_addr = u32::from(Ipv4Addr::new(255, 255, 255, 255));
			(*broadaddr).sin_zero = [0u8; 8];

			(*next).next = ptr::null_mut() as *mut raw::pcap_addr;
			(*next).addr = addr as *mut libc::sockaddr;
			(*next).netmask = netmask as *mut libc::sockaddr;
			(*next).broadaddr = broadaddr as *mut libc::sockaddr;
			(*next).dstaddr = dstaddr as *mut libc::sockaddr;

			(*raw_pcap_addr).next = next as *mut raw::pcap_addr;
			(*raw_pcap_addr).addr = ptr::null_mut() as *mut libc::sockaddr;
			(*raw_pcap_addr).netmask = ptr::null_mut() as *mut libc::sockaddr;
			(*raw_pcap_addr).broadaddr = ptr::null_mut() as *mut libc::sockaddr;
			(*raw_pcap_addr).dstaddr = ptr::null_mut() as *mut libc::sockaddr;
		}

		assert!(PcapAddr::from_raw(raw_pcap_addr).is_err());

		let _ = unsafe { libc::free(next as *mut libc::c_void) };
		let _ = unsafe { libc::free(addr as *mut libc::c_void) };
		let _ = unsafe { libc::free(netmask as *mut libc::c_void) };
		let _ = unsafe { libc::free(broadaddr as *mut libc::c_void) };
		let _ = unsafe { libc::free(raw_pcap_addr as *mut libc::c_void) };
	}
}