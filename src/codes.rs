#![allow(dead_code)]

/// # Result codes for the pcap API.
/// ## Error codes for the pcap API.
/// These will all be negative, so you can check for the success or
/// failure of a call that returns these codes by checking for a
/// negative value.
///
/// ## Warning codes for the pcap API.
/// These will all be positive and non-zero, so they won't look like
/// errors.
///
/// Source: `pcap/pcap.h`
#[derive(Eq, PartialEq, Debug)]
pub enum PcapCodes
{
    WarningTstampTypeNotsup = 3,
    WarningPromiscNotsup = 2,
    Warning = 1,
    Error = -1,
    ErrorBreak = -2,
    ErrorNotActivated = -3,
    ErrorActivated = -4,
    ErrorNoSuchDevice = -5,
    ErrorRfmonNotsup = -6,
    ErrorNotRfmon = -7,
    ErrorPermDenied = -8,
    ErrorIfaceNotUp = -9,
    ErrorCantsetTstampType = -10,
    ErrorPromiscPermDenied = -11,
    ErrorTstampPrecisionNotsup = -12
}

#[cfg(test)]
mod test
{
    use super::PcapCodes;

    #[test]
    fn codes_eq() {
        assert_eq!(PcapCodes::Error as i32, -1);
        assert_eq!(PcapCodes::Warning as i32, 1);

        assert_eq!(PcapCodes::Error, PcapCodes::Error);
        assert_eq!(PcapCodes::Warning, PcapCodes::Warning);

        assert!(PcapCodes::Error != PcapCodes::Warning);
    }
}