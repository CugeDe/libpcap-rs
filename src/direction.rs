#![allow(dead_code)]

#[derive(Eq, PartialEq, Debug)]
enum PcapDirection {
    InOut = 0,
    In = 1,
    Out = 2
}

#[cfg(test)]
mod test
{
    use super::PcapDirection;

    #[test]
    fn direction_eq() {
        assert_eq!(PcapDirection::InOut as i32, 0);
        assert_eq!(PcapDirection::Out as i32, 2);

        assert_eq!(PcapDirection::InOut, PcapDirection::InOut);
        assert_eq!(PcapDirection::Out, PcapDirection::Out);

        assert!(PcapDirection::InOut != PcapDirection::Out);
    }
}